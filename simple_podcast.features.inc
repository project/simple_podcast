<?php
/**
 * @file
 * simple_podcast.features.inc
 */

use SimplePodcastConstants as C;

/**
 * Implements hook_ctools_plugin_api().
 */
function simple_podcast_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function simple_podcast_node_info() {
  $items = array(
    'simple_podcast_feed_config' => array(
      'name' => t('Podcast Feed'),
      'base' => 'node_content',
      'description' => t('A feed configuration that renders a podcast feed based on format specified in the http request.  Select this podcast feed item in the Feeds field of any Podcast Item to have it included in this podcast feed\'s rendered output.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'simple_podcast_item' => array(
      'name' => t('Podcast Item'),
      'base' => 'node_content',
      'description' => t('A single podcast item, with transcript, audio files, and other data necessary to generate an iTunes compatible podcast feed item.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Please ensure your audio file has been edited, normalized, compressed, and includes title and duration information in the ID3 tags.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

