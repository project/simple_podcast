<?php
use SimplePodcastConstants as C;

abstract class SimplePodcastRenderer {
  // Set this constant in your subclass to override this behavior.
  CONST USE_DRUPAL_RENDER_API = false;
  public $renderings;
  public $variables;
  protected $format;
  protected $sitename;
  protected $language;
  protected $type;
  protected $node;
  protected $nodeBundle;
  protected $topics;
  protected $toolbox;
  protected $filePath;
  protected $fileExists;

  /**
   * Render using the specific plugin's rendering templates.
   *
   * @param array $variables
   *   Override variables to use in place of whatever can be loaded from the node.
   *
   * @return string
   */
  public function render(array &$variables = array()) {
    if (isset($this->variables) && is_array($this->variables)) {
      $variables = array_merge($this->variables, $variables);
      $this->variables = &$variables;
    }

    // If the template file exists and we can load all template variables
    if ($this->fileExists) {
      if (!$this->loadVariables($variables)) {
        // If unable to load variables, log but continue to process.
        watchdog('error', t('Failed to load variables for Simple Podcast.'));
      }

      // Extract variables into local scope.
      if ($this->nodeIsFeedConfiguration($variables['node'])) {
        extract($variables);
        $item_only = false;
      }
      elseif ($this->nodeIsItem($variables['node'])) {
        extract($variables, EXTR_PREFIX_ALL, 'item');
        $item_only = true;
      }

      // Prep for output capture.
      $content = '';

      // Helper variables for defining <h> tags.
      $h1 = 1;
      $h2 = 2;
      $h3 = 3;

      // If we don't want the Drupal Render API to execute at all, skip it.
      if (!static::USE_DRUPAL_RENDER_API) {
        // Delete the pending output buffer.
        while (ob_get_level() && ob_get_contents() && ob_get_clean());

        // Clear all drupal messages set.
        drupal_get_messages();

        // Allow the plugin to alter or add headers, but wait to send them to
        // allow other modules to alter the headers.
        $headers = array();
        $headers_should_override = $this->getHeaders($headers);
        drupal_send_headers($headers, $headers_should_override);

        // Load the template files directly to the output.
        include $this->filePath;

        // Terminate request to skip further Drupal Render API interaction.
        drupal_exit();
        return FALSE;
      }
      // Else if we want Drupal to handle the output via the render API, allow it.
      else {

        // Allow the plugin to alter or add headers, but wait to send them to
        // allow other modules to alter the headers.
        $headers = array();
        $headers_should_override = $this->getHeaders($headers);
        drupal_send_headers($headers, $headers_should_override);

        // Start output buffering.
        ob_start();

        // Load the template files into the output buffer.
        include $this->filePath;

        // Get the content of the rendered template files and delete the buffer.
        $content = ob_get_clean();
      }
      // Return the output as a markup element to allow the Drupal render API to render it.
      return array(
        '#type' => 'markup',
        '#markup' => $content,
      );
    }
    else {
      // If unable to load template file, log the missing filepath and return NULL to load default.
      watchdog('error', t('Failed to load template file for Simple Podcast from !path', array('!path' => $this->filePath)));
    }
    // Return NULL for error handling or default loading.
    return NULL;
  }
  protected function getFieldFirstItemUnsafeValue($node, $field_name, $lang = NULL) {
    return $this->getFieldFirstItemValue($node, $field_name, $lang, true);
  }

  protected function getFieldFirstItemValue($node, $field_name, $lang = NULL, $get_unsafe = false) {
    $l = $lang ?: $this->language;
    if (($items = field_get_items('node', $node, $field_name, $l)) && is_array($items)) {
      $item = reset($items);
      if ($get_unsafe) {
        return $item['value'];
      }
      else {
        return !empty($item['safe_value']) ? $item['safe_value'] : check_plain($item['value']);
      }
    }
    return '';
  }

  protected function ensureUnsafeFieldVariable(&$vars, $variable_name, &$node, $field_name) {
    return $this->ensureFieldVariable($vars, $variable_name, $node, $field_name, true);
  }

  protected function ensureFieldVariable(&$vars, $variable_name, &$node, $field_name, $get_unsafe = false) {
    if (empty($vars[$variable_name])) {
      $vars[$variable_name] = $this->getFieldFirstItemValue($node, $field_name, $vars['language'], $get_unsafe);
    }
    return !empty($vars[$variable_name]);
  }

  protected function ensureLanguageVariable(&$vars, &$node) {
    // If no language is set, set default language.
    if (empty($vars['language'])) {
      // If current configuration doesn't define a default language, check
      // current context for language.
      if (empty($this->language) || $this->language == LANGUAGE_NONE) {
        global $language;
        // If current context doesn't define a language, default to US English.
        if (empty($language->language) || $language->language == LANGUAGE_NONE) {
          $vars['language'] = 'en-us';
        }
        else {
          $vars['language'] = $language->language;
        }
      }
      else {
        $vars['language'] = $this->language;
      }
    }
    return !empty($vars['language']);
  }

  protected function ensureTitleVariable(&$vars, &$node) {
    if (empty($vars['title'])) {
      // Set default podcast title.
      if (isset($node->title)) {
        $vars['title'] = check_plain(node_page_title($node));
      }
      else {
        $vars['title'] = $this->sitename;
        if (!empty($vars['title'])) {
          $vars['title'] .= ' ';
        }
        $vars['title'] .= 'Podcast';
      }
    }
    return !empty($vars['title']);
  }

  protected function ensureExplicitVariable(&$vars, &$node, $field_name) {
    // If podcast configuration doesn't define whether a podcast is explicit or not, set it.
    if (empty($vars['explicit'])) {
      $explicit = $this->getFieldFirstItemUnsafeValue($node, $field_name, $vars['language']);
      if ('no' == $explicit) {
        $explicit = FALSE;
      }
      $vars['explicit'] = $explicit;
    }
    // Explicit may be 'no','yes',TRUE,FALSE,''  so it would be relatively
    // impossible to accurately check if this has been loaded or not.
    return true;
  }

  protected function ensureImageVariable(&$vars, &$node, $field_name){
    // If image url isn't set, load image.
    if (empty($vars['image_url'])) {
      if (($image = field_get_items('node', $node, $field_name, $vars['language'])) && is_array($image)) {
        $item = reset($image);
        if (!empty($item['uri'])) {
          $image_uri = $item['uri'];
          $image_url = file_create_url($item['uri']);
          $image_alt = $item['alt'] ?: $item['filename'];
        }
      }
      $vars['image_uri'] = !empty($image_uri) ? $image_uri : '';
      $vars['image_url'] = !empty($image_url) ? $image_url : '';
      $vars['image_alt'] = !empty($image_alt) ? $image_alt : false ;
    }
    return !empty($vars['image_url']);
  }

  protected function ensureCopyrightVariable(&$vars, &$node) {
    // Set default copyright.
    if (empty($vars['copyright'])) {
      $vars['copyright'] = '&#169; ' . check_plain($this->sitename);
    }
    return !empty($vars['copyright'])
    && $vars['copyright'] != '&#169; ';
  }

  protected function ensureConfigLinkVariable(&$vars, &$node) {
    // Set default link path
    if (empty($vars['link'])) {
      $path = isset($node->nid) ? 'node/' . $node->nid : NULL;
      global $base_url;
      global $base_path;
      $vars['link'] = $base_url . $base_path . drupal_get_path_alias($path);
    }
    return !empty($vars['link']);
  }

  protected function ensureConfigTitleVariable(&$vars, &$node) {
    return $this->ensureTitleVariable($vars, $node);
  }

  protected function ensureConfigExplicitVariable(&$vars, &$node) {
    return $this->ensureExplicitVariable($vars, $node, C::FIELDS_CONFIG_EXPLICIT);
  }

  protected function ensureConfigCopyrightVariable(&$vars, &$node) {
    return $this->ensureCopyrightVariable($vars, $node);
  }

  protected function ensureConfigAuthorVariable(&$vars, &$node) {
    return $this->ensureFieldVariable($vars, 'author', $node, C::FIELDS_CONFIG_AUTHOR);
  }

  protected function ensureConfigSummaryVariable(&$vars, &$node){
    // If summary isn't set, load summary.  Summary allows HTML, so don't
    // check_plain this. The Forms API will ensure this data is safe to use.
    return $this->ensureFieldVariable($vars, 'summary', $node, C::FIELDS_CONFIG_SUMMARY);
  }

  protected function ensureConfigImageVariables(&$vars, &$node) {
    return $this->ensureImageVariable($vars, $node, C::FIELDS_CONFIG_COVER_ART);
  }

  protected function ensureConfigDescriptionVariable(&$vars, &$node) {
    // If description isn't set, load description.  Description allows HTML, so don't
    // check_plain this. The Forms API will ensure this data is safe to use.
    return $this->ensureFieldVariable($vars, 'description', $node, C::FIELDS_CONFIG_DESCRIPTION);
  }

  protected function ensureConfigOwnerVariable(&$vars, &$node) {
    if (empty($vars['owner'])) {
      $vars['owner'] = array('name' => '', 'email' => '');
    }
    $owner = &$vars['owner'];

    // If either name or email are empty, and we have a uid to work with, get owner.
    if ((empty($owner['name']) || empty($owner['email'])) && !empty($node->uid)) {
      $o = field_get_items('node',$node,C::FIELDS_CONFIG_OWNER);
      if (is_array($o)) {
        $o = reset($o);
        if (!empty($o['entity'])) {
          $o = $o['entity'];
        }
        elseif (!empty($o['target_id'])) {
          $o = user_load($o['target_id']);
        }
      }
      if (!empty($o->name)) $owner['name'] = $o->name;
      if (!empty($o->mail)) $owner['email'] = $o->mail;
    }
    return !empty($vars['owner'])
      && !empty($vars['owner']['name'])
      && !empty($vars['owner']['email']);
  }

  protected function ensureItemCategoriesVariable(&$vars, &$node_item) {
    if (empty($vars['categories']) || !is_array($vars['categories'])) {
      $vars['categories'] = array();

      $feed_nodes = $this->getParentFeeds($node_item, $this->language);
      if (is_array($feed_nodes)) {
        foreach ($feed_nodes as $node) {
          $classes = [];
          $classes[] = "podcast-item-category";
          if (!empty($node->title)) {
            $title = drupal_clean_css_identifier(strtolower($node->title));
            if ($title) {
              $classes[] = "podcast-item-category-" . $title;
            }
          }
          $link = l($node->title, "node/$node->nid");
          if (!empty($link)) {
            $vars['categories'][$link] = implode(' ', $classes);
          }
        }
      }
    }
  }
  protected function ensureConfigCategoriesVariable(&$vars, &$node) {
    // If categories aren't set, get the terms stored in the hierarchical select
    // taxonomy term field, and nest them in order.  We only allow a single
    // selected value, so the field will contain an ordered list of the term's
    // ancestors, ending with the selected term.
    if (empty($vars['categories']) || !is_array($vars['categories'])) {
      $field_terms = field_get_items('node', $node, C::FIELDS_CONFIG_TOPIC, $vars['language']);
      $vars['categories'] = array();

      if (!empty($field_terms) && is_array($field_terms)) {
        foreach ($field_terms as $field_term) {
          $tids[] = $field_term['tid'];
        }
      }

      if (!empty($tids)) {
        $categories = taxonomy_term_load_multiple($tids);
        $categories_nested = array();

        if (!empty($categories) && is_array($categories)) {
          $parent = &$categories_nested;
          foreach ($tids as $tid) {
            if (!empty($categories[$tid]->name)) {
              $name = check_plain($categories[$tid]->name);
              $parent[$name] = array();
              $parent = &$parent[$name];
            }
          }
        }
        $vars['categories'] = $categories_nested;
      }

      if (!empty($var['categories'])) {
        $vars['categories'] = array();
      }
    }
    return !empty($vars['categories']);
  }

  protected function ensureConfigItemsVariable(&$vars, &$config_node) {
    $efq = $this->getChildrenEntityFieldQuery($config_node);
    $efq->pager = FALSE;
    $results = $efq->execute();

    if (!empty($results['node'])) {
      $vars['items'] = $results['node'];
    }
    else {
      $vars['items'] = array();
    }

    // Since having zero items is technically considered a valid podcast feed,
    // return true regardless of if any items were found or not.
    return true;
  }

  protected function ensureConfigSubscribeVariable(&$vars, &$node) {
    // "Subscribe" is a field collection with infinite possible values, so we need
    // to grab each one using entity_load, then process their values.
    $subscriptions = field_get_items('node', $node, C::FIELDS_CONFIG_SUBSCRIBE, $vars['language']);
    if ($subscriptions) {
      // Strip the protocol for easy use.
      $base_url = preg_replace('/^http(s)?:\/\//i', '', $GLOBALS['base_url']);
      $base_url = $base_url . '/' . drupal_get_path_alias('node/' . $node->nid);
      foreach ($subscriptions as $subscribe_key => $subscribe_id) {
        // Each subscription value is stored as an entity reference, which we load
        // from field collection's entity table.
        $subscribe_entity = entity_load_single('field_collection_item', $subscribe_id['value']);
        if (isset($subscribe_entity->field_simple_podcast_sub_type[LANGUAGE_NONE][0]['value'])) {
          $subscribe_type = $subscribe_entity->field_simple_podcast_sub_type[LANGUAGE_NONE][0]['value'];
          $class_array = array(
            'simple-podcast-subscribe-link',
            'simple-podcast-subscribe-link-' . $subscribe_type,
          );
          // Currently there are three options for subscription: iTunes, XML, and Custom Link.
          switch ($subscribe_type) {
            case 'itpc':
              // Format a subscribe link that goes to an iTunes-style link.
              $vars['subscribe'][] = l(t('Subscribe via iTunes'), 'itpc://' . $base_url, array(
                'attributes' => array(
                  'class' => $class_array,
                ),
                'external' => TRUE,
                'query' => array(
                  'format' => 'itunes',
                ),
              ));
              break;

            case 'xml':
              // Format a subscribe link to printing out the flat XML, which RSS feeds can subscribe to.
              $vars['subscribe'][] = l(t('Subscribe via RSS'), 'http://' . $base_url, array(
                'attributes' => array(
                  'class' => $class_array,
                ),
                'external' => TRUE,
                'query' => array(
                  'format' => 'itunes',
                ),
              ));
              break;

            case 'custom':
              // Print out the editor-specified custom link.
              $link_info = $subscribe_entity->field_simple_podcast_sub_link[LANGUAGE_NONE][0];
              $vars['subscribe'][] = l($link_info['title'], $link_info['url'], array(
                'attributes' => $link_info['attributes'] + array(
                    'class' => $class_array,
                  ),
                'external' => TRUE,
              ));
              break;

          }
        }
      }
    }
    else {
      $vars['subscribe'] = array();
    }
    return !empty($vars['subscribe']);
  }

  /**
   * Return Drupal entity nodes of SimplePodcastFeedConfig that are the parents of a given SimplePodcastItem.
   *
   * @param $node_item
   *   The node object of the SimplePodcastItem whose parent is desired.
   * @param $language
   *   The language code of the language desired.
   * @return array
   *   An array of node objects of the feed parents.
   */
  protected function getParentFeeds($node_item, $language) {
    $nodes = array();
    $nids = array();
    $feeds = field_get_items('node', $node_item, C::FIELDS_ITEM_FEED, $language);

    if (is_array($feeds)) {
      foreach ($feeds as $key => $feed) {
        $nid = $feed['target_id'];
        if (!empty($nid)) {
          $nids[$nid] = $nid;
        }
      }
      $nodes = node_load_multiple($nids);
    }

    return $nodes;
  }

  protected function getChildrenEntityFieldQuery($node) {
    return
      (new EntityFieldQuery())
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', C::BUNDLE_ITEM, 'CONTAINS')
        ->propertyCondition('status', NODE_PUBLISHED)
        ->fieldCondition(C::FIELDS_ITEM_FEED, 'target_id', $node->nid)
        // Order with new episodes at the top.
        ->propertyOrderBy('created', 'DESC')
        // Add a tag so others can use hook query alter, if they prefer, rather
        // than subclassing the plugin and overriding this function to alter the
        // query structure.
        ->addTag(C::QUERY_TAG_CHILDREN);
  }

  /**
   * Get a render cache object.
   *
   * @param $nids
   *   Node IDs for which cached data should be retrieved. This array is
   *   modified during this function such that only NIDs that fail to find
   *   cache entries remain in the NID list.
   *
   * @return SimplePodcastRenderCache
   */
  protected function getChildrenFromCache(&$nids) {
    // Attempt to get cached data for rendering the entities.
    return new SimplePodcastRenderCache($this->format, $nids);
  }

  /**
   * @param $nodes
   *   Array of fully loaded nodes, keyed by Node ID.
   *
   * @param SimplePodcastEntityCache $cache
   *
   * @param array $nids
   *   Node IDs array that will be modified to remove NIDs that successfully
   *   render.  The count of $nids must match the count of $nodes or $nids will
   *   be overwritten by the keys of $nodes.
   *
   * @throws Exception
   */
  protected function addChildrenToCache(array $nodes, $cache, array &$nids = null) {

    $c = count($nodes);

    if (null === $nids || $c != count($nids)) {
      $nids = array_keys($nodes);
    }
    $i = 0;
    $h1 = 4;
    $h2 = 5;
    $h3 = 6;

    $nids_successfully_cached = array();
    foreach ($nodes as $nid => $node) {
      $vars = array();
      $vars['language'] = $this->language;
      // NOTE: Important - Do not set $vars['node'] before calling this function
      // or you will get odd behaviors.
      if (!$this->loadItemVariables($vars, $node)) {
        watchdog('error', 'Podcast Item [Nid: ' . $nid . '] failed to load all item variables.');
      }

      extract($vars, EXTR_PREFIX_ALL, 'item');

      // This is being rendered as a child of the config item, not as a podcast item
        $item_only = FALSE;

      // Prep a safe numeric ID for children
      ++$i;

      // Start output buffering
      ob_start();
      include $this->getItemTemplateFilePath();

      // Retrieve buffered output as string we can cache
      $rendered = ob_get_clean();

      if (FALSE === $rendered) {
        // Impossible to reach, but if output buffering fails to start...
        throw new Exception('Unable to render item template file.');
      }
      // Add the rendering to the cache
      $cache->cache_set($nid, $rendered);
      unset($nodes[$nid]);
      $nids_successfully_cached[] = $nid;
    }
    $nids = array_diff($nids, $nids_successfully_cached);
  }

  protected function flushRenderings() {
    if (!empty($this->renderings)) {
      $n = "\n";
      foreach ($this->renderings as $cid => $rendering) {
        print $rendering;
        print $n;
        unset($this->renderings[$cid]);
      }
    }
  }

  protected function renderChildren($items) {
    // Attempt to load from cache.
    $nids = array_keys($items);
    $cache = $this->getChildrenFromCache($nids);

    // If NIDs is not empty, then some nodes still need to be rendered and cached.
    if (!empty($nids)) {
      $nodes = node_load_multiple($nids);
      $this->addChildrenToCache($nodes, $cache, $nids);
    }

    if (!empty($nids)) {
      // TODO: add logging of nodes that error.
    }

    // Insert or update the plugin's pending renderings.
    $this->renderings = $cache->data + $this->renderings;

    // Ensure that if no items are loaded, the file is still formed correctly
    if (empty($this->renderings)) {
      $this->renderings[] = "\n";
    }
    $this->flushRenderings();
  }

  protected function ensureItemFileVariables(&$vars, &$node) {
    $file_info_missing = FALSE;
    foreach (['duration', 'url', 'mimetype', 'bytes', 'type'] as $key) {
      // If array key is empty, set file_info_missing to true and exit the loop.
      if (empty($vars['file'][$key])) {
        $file_info_missing = TRUE;
        break;
      }
    }

    // If file url isn't set, retrieve podcast data and populate the variables.
    if ($file_info_missing) {
      $podcast_info = simple_podcast_get_extra_field_data($node);

      $vars['file']['duration'] = !empty($podcast_info->duration->formatted) ? $podcast_info->duration->formatted : '';
      $vars['file']['url'] = !empty($podcast_info->url) ? $podcast_info->url : '';
      $vars['file']['mimetype'] = !empty($podcast_info->mimetype) ? $podcast_info->mimetype : '';
      $vars['file']['bytes'] = !empty($podcast_info->filesize->data) ? $podcast_info->filesize->data : '';
      $vars['file']['megabytes'] = !empty($podcast_info->filesize->formatted) ? $podcast_info->filesize->formatted : '';
      $mime_parts = explode('/', $vars['file']['mimetype']);
      $vars['file']['type'] = (string)reset($mime_parts);
    }
    return !empty($vars['file']['duration'])
      && !empty($vars['file']['url'])
      && !empty($vars['file']['mimetype'])
      && !empty($vars['file']['bytes'])
      && !empty($vars['file']['megabytes']);
  }

  protected function ensureItemTitleVariable(&$vars, &$node) {
    return $this->ensureTitleVariable($vars, $node);
  }

  protected function ensureItemGUIDVariable(&$vars, &$node) {
    // If GUID isn't set, set default GUID.
    if (empty($vars['guid'])) {
      $vars['guid'] = !empty($vars['file']['url']) ? $vars['file']['url'] : 'error-' . $node->nid . 'x' . md5('podcast-feed-' . $node->created);
    }
    return !empty($vars['guid']);
  }

  protected function ensureItemAuthorVariable(&$vars, &$node) {
    return $this->ensureFieldVariable($vars, 'author', $node, C::FIELDS_ITEM_AUTHOR);
  }

  protected function ensureItemSummaryVariable(&$vars, &$node) {
    // If summary isn't set, load summary.  Summary allows HTML, so don't
    // check_plain this. The Forms API will ensure this data is safe to use.
    return $this->ensureFieldVariable($vars, 'summary', $node, C::FIELDS_ITEM_SUMMARY);
  }

  protected function ensureItemDescriptionVariable(&$vars, &$node) {
    // If description isn't set, load summary.  Summary allows HTML, so don't
    // check_plain this. The Forms API will ensure this data is safe to use.
    return $this->ensureFieldVariable($vars, 'description', $node, C::FIELDS_ITEM_DESCRIPTION);
  }

  protected function ensureItemImageVariables(&$vars, &$node){
    return $this->ensureImageVariable($vars, $node, C::FIELDS_ITEM_COVER_ART);
  }
  protected function ensureItemCopyrightVariable(&$vars, &$node) {
    return $this->ensureCopyrightVariable($vars, $node);
  }

  protected function ensureItemPublishedVariable(&$vars, &$node) {
    if (empty($vars['published'])) {
      if (!empty($node->published_at)) {
        $vars['published'] = $node->published_at;
      }
      else {
        $vars['published'] = $node->created;
      }
    }
    return !empty($vars['published']);
  }

  protected function ensureItemExplicitVariable(&$vars, &$node) {
    return $this->ensureExplicitVariable($vars, $node, C::FIELDS_ITEM_EXPLICIT);
  }

  protected function ensureItemTranscriptVariable(&$vars, &$node) {
    return $this->ensureFieldVariable($vars, 'transcript', $node, C::FIELDS_ITEM_TRANSCRIPT);
  }

  protected function ensureItemSubscribeVariable(&$vars, &$node) {
    // If subscribe is not set, load it from parent Config Variable.
    if (!array_key_exists('subscribe', $vars) || !is_array($vars['subscribe'])) {
      $vars['subscribe'] = array();

      $feed_nodes = $this->getParentFeeds($node, $this->language);
      if (is_array($feed_nodes)) {
        foreach ($feed_nodes as $feed_node) {
          // Pass the parent as the node to get subscribe info from, but the item receives the $vars updates.
          $this->ensureConfigSubscribeVariable($vars, $feed_node);
        }
      }
    }
    return !empty($vars['subscribe']);
  }

  /**
   * Load Podcast Item variables.
   *
   * @param array $vars
   *   Variables array.
   *
   * @param null $node
   *   Node to use instead of plugin's currently defined node. Leave null to use
   *   node defined during instantiation of the plugin class.
   *
   * @throws Exception
   *
   * @returns bool
   *   True if all variables loaded, false otherwise.
   */
  protected function loadItemVariables(array &$vars, &$node = null) {
    if (null == $node && !empty($vars['node'])) {
      $node = $vars['node'];
    }
    if (null === $node) {
      return false;
    }
    if (empty($node->nid)){
      return false;
    }

    $vars['path'] = url('node/' . $node->nid);

    $vars['classes'][] = 'simple-podcast-item';

    // Ensure language before doing anything.
    $has_lang = $this->ensureLanguageVariable($vars, $node);

    // Process any non-required fields
    // Transcript, Categories, Copyright, and Subscribe are not required for all renderings.
    $this->ensureItemTranscriptVariable($vars, $node);
    $this->ensureItemCategoriesVariable($vars, $node);
    $this->ensureItemCopyrightVariable($vars, $node);
    $this->ensureItemSubscribeVariable($vars, $node);

    // All of the below are required, so return a single true statement only if
    // all of these return true. Do each check on own line to facilitate debugging.
    $success = $has_lang;
    $success = $this->ensureItemFileVariables($vars, $node) && $success;
    $success = $this->ensureItemGUIDVariable($vars, $node) && $success;
    $success = $this->ensureItemTitleVariable($vars, $node) && $success;
    $success = $this->ensureItemAuthorVariable($vars, $node) && $success;
    $success = $this->ensureItemSummaryVariable($vars, $node) && $success;
    $success = $this->ensureItemDescriptionVariable($vars, $node) && $success;
    $success = $this->ensureItemImageVariables($vars, $node) && $success;
    $success = $this->ensureItemPublishedVariable($vars, $node) && $success;
    $success = $this->ensureItemExplicitVariable($vars, $node) && $success;

    return $success;
  }

  /**
   * Load Podcast Feed Configuration variables.
   *
   * @param array $vars
   *   Variables array.
   *
   * @param null $node
   *   Node to use instead of plugin's currently defined node. Leave null to use
   *   node defined during instantiation of the plugin class.
   *
   * @throws Exception
   */
  protected function loadConfigVariables(array &$vars, &$node = null) {
    if (NULL == $node) {
      if (!empty($vars['node'])) {
        $node =& $vars['node'];
      }
      else if (!empty ($this->node)) {
        $node =& $this->node;
      }
      else {
        throw new Exception("Unable to process configuration, $node is null.");
      }
    }
    $vars['qformat'] = $this->format;

    // Ensure language before doing anything.
    $has_lang = $this->ensureLanguageVariable($vars, $node);

    // Process any non-required fields.
    $this->ensureConfigSubscribeVariable($vars, $node);

    // All of the below are required, so return a single true statement only if
    // all of these return true. Do each check on own line to facilitate debugging.
    $success = $has_lang;
    $success = $this->ensureConfigTitleVariable($vars, $node) && $success;
    $success = $this->ensureConfigLinkVariable($vars, $node) && $success;
    $success = $this->ensureConfigExplicitVariable($vars, $node) && $success;
    $success = $this->ensureConfigCopyrightVariable($vars, $node) && $success;
    $success = $this->ensureConfigAuthorVariable($vars, $node) && $success;
    $success = $this->ensureConfigSummaryVariable($vars, $node) && $success;
    $success = $this->ensureConfigDescriptionVariable($vars, $node) && $success;
    $success = $this->ensureConfigOwnerVariable($vars, $node) && $success;
    $success = $this->ensureConfigImageVariables($vars, $node) && $success;
    $success = $this->ensureConfigCategoriesVariable($vars, $node) && $success;
    $success = $this->ensureConfigItemsVariable($vars, $node) && $success;

    return $success;
  }

  /**
   * Dynamically load default variables for either an item or a feed configuration.
   *
   * @param array $variables
   *   Array of prepopulated variables which will not be overridden.
   *
   * @throws Exception
   */
  public function loadVariables(array &$variables) {
    // Ensure variables
    if (empty($variables)) {
      $variables = array();
    }

    if (empty($variables['node']) && !empty($this->node)) {
      $variables['node'] =& $this->node;
    }
    if (empty($variables['classes'])) {
      $variables['classes'] = array();
    }
    if (!is_array($variables['classes'])) {
      $class = $variables['classes'];
      $variables['classes'] = array();
      $variables['classes'][] = $class;
    }

    $variables['classes'][] = 'simple-podcast-element';

    $vars_loaded = false;
    if ($this->nodeIsItem($variables['node'])) {
      $variables['classes'][] = 'simple-podcast-page';
      $variables['classes'][] = 'simple-podcast-item';
      $vars_loaded = $this->loadItemVariables($variables);
    }
    elseif ($this->nodeIsFeedConfiguration($variables['node'])) {
      $variables['classes'][] = 'simple-podcast-feed';
      $vars_loaded = $this->loadConfigVariables($variables);
    }
    return $vars_loaded && !empty($variables['node']);
  }

  /**
   * Get the location of the plugin's listing template file.
   *
   * @return string
   *   The location of the plugin's listing template file.
   */
  public abstract function getListTemplateFilePath();

  /**
   * Get the location of the plugin's item template file.
   *
   * @return string
   *   The location of the plugin's item template file.
   */
  public abstract function getItemTemplateFilePath();

  /**
   * Return headers to send for the request.
   *
   * If no action is required by your plugin, simply implement an empty function.
   *
   * @returns bool
   *   True if headers should overwrite, false if headers should just be added.
   */
  public abstract function getHeaders(array &$headers);

  /**
   * Dynamically get the file template's path based on node's type.
   *
   * @param Entity|null|stdClass $node
   *   Node-like object, e.g. { nid: "123", type: "simple_podcast_item" }
   *
   * @return bool|string
   *   The path to the file, or FALSE if the node is not a valid node type.
   */
  public function getTemplateFilePath($node = null){
    if (null === $node) {
      $node = $this->node;
    }
    if ($this->nodeIsItem($node)) {
      return $this->getItemTemplateFilePath($node);
    }
    if ($this->nodeIsFeedConfiguration($node)) {
      return $this->getListTemplateFilePath($node);
    }
    return FALSE;
  }

  /**
   * Determines if node is considered a podcast feed configuration item.
   *
   * @return bool
   */
  public function nodeIsFeedConfiguration($node) {
    return (false !== strpos($node->type, 'simple_podcast_feed_config'));
  }

  /**
   * Determines if node is considered a podcast item.
   *
   * @return bool
   */
  public function nodeIsItem($node) { //TODO: make this a strpos check
    return (false !== strpos($node->type, 'simple_podcast_item'));
  }

  /**
   * Create a Podcast Renderer object
   *
   * @param Entity|null|stdClass $node
   *   Node-like object, e.g. { nid: "123", type: "simple_podcast_item" }
   *
   * @param string $language
   *   (optional) Drupal language value for default language to use in accessing
   *   field data and for the podcast's defined language. If none is supplied,
   *   the default is LANGUAGE_NONE, and the default language for the Podcast
   *   will be 'us-en'.
   *
   * @throws Exception
   */
  public function __construct ($node, $language = LANGUAGE_NONE, $format='xml') {
    if (!empty($node) && !empty($node->nid) && !empty($node->type)) {
      $this->node = $node;
      $this->filePath = $this->getTemplateFilePath();
      $this->fileExists = ($this->filePath && is_readable($this->filePath));
      $this->renderings = array();
      $this->sitename = variable_get('site_name', '');
      $this->format = $format;

      // Ensure language is a string value, not a language object.
      if (!empty($language->language)) { $language = $language->language; }
      $this->language = $language;

      if (!$this->fileExists) {
        throw new Exception('Error, Simple Podcast Type template files cannot be loaded.');
      }
    }
    else {
      throw new Exception(
        empty($node) ? 'Node is empty.' :
          (empty($node->nid) ? 'NID is missing. ' : "NID [$node->nid] ") .
          (empty($node->type) ? 'type is empty. ' : "type [$node->type]")
      );
    }
  }
}

