<?php
use SimplePodcastConstants as C;
abstract class SimplePodcastEntityCache {
  // The base abstract class is defined with no suffix so that clearing cache
  // using its static functions clears all classes that inherit from it.
  const CID_SUFFIX = '';
  const CACHEBIN = 'cache_simplepodcast_entity_cache';

  public $key;
  /**
   * @var array
   */
  public $data;

  /**
   * Instantiate a cache tool to cache data related to an entity.
   *
   * @param $key
   *   Unique key to differentiate the various entity based caches.
   *
   * @param array $nids
   *   (optional) Node IDs to load. Leave empty to simply instantiate an empty cache object.
   *
   * @param bool $are_cids
   *  (optional) If $nids array actually contains full cache IDs, set this to true.
   */
  public function __construct($key='all', array &$nids = NULL, $are_cids = false) {
    $this->key = $key;
    $this->data = array();
    if (!empty($nids)) {
      $this->cache_get_multiple($nids, $are_cids);
    }
  }

  /**
   * @param $cid
   * @param string $default
   * @return string
   */
  protected function _cache_get($cid, $default = '') {
    $cache = cache_get($cid, static::CACHEBIN);
    if (isset($cache->data)) {
      $this->data[$cid] = $cache->data;
      return $cache->data;
    }
    else {
      return $default;
    }
  }

  /**
   * @param $cids
   *   Implicitly indexed array of cache IDs. This array will be modified to
   *   contain only those cache IDs that did not return data.
   *
   * @returns array
   *   Array of data, keyed by cache ID.
   */
  protected function _cache_get_multiple(&$cids) {
    // Set up a separate array to use for sorting the found cache items.
    $cache_ids = $cids;
    $data = array();

    if (is_array($cids) && !empty($cids)) {
      $cache = cache_get_multiple($cids, static::CACHEBIN);

      // Sort the found items based on their position in cids, since
      // cache_get_multiple() always returns sorted on nid.
      foreach ($cache_ids as $key => $cid) {
        if (isset($cache[$cid])) {
          $data[$cid] = $cache[$cid]->data;
        }
      }
      // Reset array index in case some cids returned no values.
      $cids = array_values($cids);
    }

    return $data;
  }

  protected function _cache_set($cid, $data) {
    cache_set($cid, $data, static::CACHEBIN, CACHE_PERMANENT);
    $this->data[$cid] = $data;
  }

  /**
   * Get the cache ID object for a given NID and plugin hook.
   *
   * @param $nid
   *   Node ID for which to retrieve the cache ID object.
   *
   * @param bool $is_cid
   *   If $nid is actually a full cache ID string, set this to true.
   *
   * @return SimplePodcastCacheID
   *   The cache ID for a given NID and plugin hook.
   */
  public function getCIDObject($nid, $is_cid = false) {
    $format = ($is_cid) ? false : $this->key;
    return new SimplePodcastCacheID($nid, $format, static::CID_SUFFIX);
  }

  /**
   * Get the string representation of a cache ID for a given NID and plugin.
   *
   * @param $nid
   *   Node ID for which to retrieve cache ID as string.
   *
   * @return string
   */
  public function getCID($nid){
    $d = $this->getCIDObject($nid);
    if (isset($d->cid)){
      return $d->cid;
    }
    else {
      return '';
    }
  }

  /**
   * Retrieve Cache ID objects for a specific plugin for a given set of node ids.
   *
   * @param array $nids
   *   An array of Node ID for which to retrieve Cache ID objects.
   *
   * @param bool $are_cids
   *  If $nids is actually an array of full cache IDs, set this to true.
   *
   * @return SimplePodcastCacheID[]|string[]
   */
  public function getCIDObjects(array $nids, $are_cids = false) {
    $c = count($nids);
    for ($i = 0; $i < $c; ++$i) {
      $nids[$i] = $this->getCIDObject($nids[$i], $are_cids);
    }
    return $nids;
  }

  /**
   * Get string array of Cache IDs for given array of Node IDs and plugin.
   *
   * @param array $nids
   *   An implicitly indexed array of Node IDs.
   *
   * @return string[]
   *   An array of Cache IDs as strings.
   */
  public function getCIDs(array $nids){
    $c = count($nids);
    for ($i = 0; $i < $c; ++$i) {
      $nids[$i] = $this->getCID($nids[$i]);
    }
    return $nids;
  }

  /**
   * Get the Node ID for a given Cache ID.
   *
   * @param $cid
   *
   * @return integer|null
   *
   */
  public function getNID($cid) {
    $data = $this->getNIDs(array($cid));
    return reset($data);
  }

  /**
   *
   * @param array $cids
   * @return array
   */
  public function getNIDs(array $cids) {
    $data = $this->getCIDObjects($cids, TRUE);
    $nids = array();
    $c = count($data);
    for ($i = 0; $i < $c; ++$i) {
      $nids[] = $data[$i]->nid;
    }
    return $nids;
  }

  /**
   * @param $nid
   *   $nid, or full Cache ID.
   *
   * @param bool $is_cid
   *   If $nid is actually the full cid, set this to true.
   *
   * @return string
   */
  public function cache_get($nid, $is_cid = false) {
    $r = '';
    if ($is_cid) {
      $d = $this->_cache_get($nid);
      if (!empty($d)) {
        $r = $d;
      }
    }
    else {
      $cid = $this->getCID($nid, $is_cid);
      $d = $this->_cache_get($cid);
      if (!empty($d)) {
        $r = $d;
      }
    }

    return $r;
  }

  /**
   * @param $nid
   *   The Node ID or Cache ID.
   * @param bool $is_cid
   *   If $nid is actually the full cache id, set this to true.
   */
  public function cache_set($nid, $data, $is_cid = false) {
    $cid = ($is_cid ? $nid : $this->getCID($nid));
    $this->_cache_set($cid, $data);
  }

  /**
   * Set the cache for multiple items at once.
   *
   * @param array $data
   *   An array of data for the key specified, keyed by NID
   *
   * @param bool $are_cids
   *   If $data is actually keyed by the full cache ID, set this to true.
   *
   * @return $this
   *   Return a self-reference to allow chaining.
   */

  public function cache_set_multiple(array $data, $are_cids = false) {
    foreach($data as $nid => $item) {
      $this->cache_set($nid, $item, $are_cids);
    }
    return $this;
  }

  /**
   * Get multiple cached items based on passed in array of cache IDs.
   *
   * @param array $cids
   *   An implicitly indexed array of cache IDs.
   *   NOTE: Any cache that successfully retrieves a value will be removed
   *         from this array.
   *
   * @param bool $are_cids
   *   If $nids are actually full cache ids, set this to true.
   *
   * @return SimplePodcastRenderCache
   *   Returns current object for function chaining.
   */
  public function cache_get_multiple(array &$nids, $are_cids = false) {
    if ($are_cids) {
      $data = $this->_cache_get_multiple($cids);
    }
    else {
      $cids = $this->getCIDs($nids);
      $data = $this->_cache_get_multiple($cids);
      if (!empty($cids)){
        // Convert back to NIDs:
        $nids = $this->getNIDs($cids);
      }
      else {
        $nids = array();
      }
    }



    // If we successfully retrieved data,
    if (is_array($data)) {
      // Use union operator (instead of array_merge) so the array index isn't
      // munged if the cache ID is numeric.
      $this->data = $data + $this->data;
    }

    return $this;
  }

  public static function clearAll($cache_bin = NULL){
    if (NULL === $cache_bin) {
      $cache_bin = static::CACHEBIN;
    }
    cache_clear_all('*', $cache_bin, TRUE);
  }

  /**
   * @param $node
   *   Node object for which to clear the cache.
   * @param null $suffix
   *   Suffix for identifying the specific subset of the entity cache.  If empty,
   *   the current class object's default suffix is used.  If '*', all cached
   *   values related to the entity are cleared.
   * @param null $cache_bin
   *   Cache bin to clear.  If empty, the CACHEBIN constant from the current
   *   class object wil be the bin cleared.
   * @return bool
   */
  public static function clearNode($node, $suffix = NULL, $cache_bin = NULL) {

    // Ensure node is valid.
    if (empty($node->nid)) {
      return FALSE;
    }

    // Ensure suffix is set, or default to current class suffix.
    if (NULL === $suffix) {
      $suffix = static::CID_SUFFIX;
    }
    // If suffix was defined as '*', set suffix to empty string so cid passed to
    // cache_clear_all will be in the format of "nid:format:" causing wildcard
    // to match and clear all suffixes.
    elseif ('*' == $suffix) {
      $suffix = '';
    }

    // Ensure cache bin is set, or default to current class cache bin.
    if (NULL === $cache_bin) {
      $cache_bin = static::CACHEBIN;
    }

    $nid = $node->nid;

    // Get all plugin formats.
    $plugins = (new SimplePodcastPluginList())->getPlugins();
    $plugin_formats = array_keys($plugins);

    // Clear the entity cache for current node for all formats.
    foreach($plugin_formats as $format) {
      $cid = (new SimplePodcastCacheID($nid, $format, $suffix))->cid;
      cache_clear_all($cid, $cache_bin, true);
    }

    // If node is a podcast item, look for podcast config items that may have
    // their rendering cached, and clear the cache for those as well.
    if (FALSE !== strpos($node->type, C::BUNDLE_ITEM)) {
      $items = field_get_items('node', $node, C::FIELDS_ITEM_FEED);
      if (is_array($items)) {
        foreach ($items as $item) {
          if (!empty($item['target_id'])) {
            $nid = $item['target_id'];
            foreach($plugin_formats as $format) {
              $cid = (new SimplePodcastCacheID($nid, $format, $suffix))->cid;
              cache_clear_all($cid, $cache_bin, true);
            }
          }
        }
      }
    }
    return TRUE;
  }
}
