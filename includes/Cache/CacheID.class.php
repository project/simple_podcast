<?php
class SimplePodcastCacheID {

  public $suffix;
  public $format;
  public $nid;
  public $cid;

  /**
   * Construct an object representing a Simple Podcast cache id.
   *
   * @param $cid_or_nid
   *   If $suffix and $plugin_hook are specified, NID, else full Cache ID.
   *
   * @param string $format
   *   (optional) Plugin hook.
   *   i.e. If URL query is '?format=xml' then the format would be 'xml' to get
   *        the cache item associated with that url rendering.
   *
   * @param string $suffix
   *   (optional) Cache ID suffix.
   *
   * @throws Exception
   *   If suffix or plugin are not defined, and the NID/CID cannot be parsed as
   *   a CID, an exception is thrown.
   */
  public function __construct($cid_or_nid, $format = NULL, $suffix = NULL) {
    if (!empty($format) && !empty($suffix)) {
      $this->suffix = $suffix;
      $this->format = $format;
      $this->nid = $cid_or_nid;
      $this->cid = $this->__toString();
    }
    else {
      $data = explode(':', $cid_or_nid);
      $this->suffix = array_pop($data);
      $this->nid = array_pop($data);
      $this->format = array_pop($data);
      $this->cid = $cid_or_nid;
      if ($this->suffix == $cid_or_nid) {
        throw new Exception('Invalid Simple Podcast CID: ' . $cid_or_nid);
      }
    }
  }

  /**
   * @return string
   *   The string representation of the cache id.
   *   e.g. [
   *     xml:1234
   */
  public function __toString() {
    return ($this->format . ':'  . $this->nid . ':' . $this->suffix) ?: '';
  }

}
