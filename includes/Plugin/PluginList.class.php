<?php
class SimplePodcastPluginList {
  protected $plugins;

  public function getPluginClassName($format) {
    $plugins = $this->getPlugins();

    // If unable to retrieve the class, return null
    if (empty($plugins[$format]->pluginClassName)) return NULL;

    // If the class doesn't extend SimplePodcastRenderer, return null
    if (!is_subclass_of($plugins[$format]->pluginClassName, SimplePodcastRenderer::class)) return NULL;

    // Return the plugin class name
    $classname = $plugins[$format]->pluginClassName;
    return $classname;
  }

  /**
   * @param $format
   * @param $node
   * @param string $language
   * @return SimplePodcastRenderer
   *   Returns the appropriate plugin that extends the SimplePodcastRenderer class.
   */
  public function getPlugin($format, $node, $language = LANGUAGE_NONE) {
    $class = $this->getPluginClassName($format);
    return new $class($node, $language, $format);
  }

  /***
   * @return SimplePodcastPluginInfo[]
   */
  public function getPlugins() {

    if (empty($this->plugins)) {
      // Load default plugins
      $this->plugins = simple_podcast_plugins_hook();
    }

    // Ensure value is instantiated and is an array before returning.
    if (!is_array($this->plugins)) {
      $this->plugins = array();
    }
    $plugins = $this->plugins;
    return $plugins;
  }

  public function __construct() {
    // initialize the plugins
    $this->getPlugins();
  }
}
