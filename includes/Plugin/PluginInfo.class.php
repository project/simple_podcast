<?php
class SimplePodcastPluginInfo {

  public $endpointTrigger;
  public $pluginClassName;
  public function __construct($endpoint_trigger, $plugin_class_name) {
    if (is_subclass_of($plugin_class_name, 'SimplePodcastRenderer')) {
      if (empty($endpoint_trigger)) {
        throw new Exception('Error, endpoint trigger must be defined!');
      }
      $this->endpointTrigger = $endpoint_trigger;
      $this->pluginClassName = $plugin_class_name;
    }
    else {
      throw new Exception('Error, Invalid SimplePodcastPluginInfo [trigger: ' . $endpoint_trigger . '][classname: '. $plugin_class_name . ']');
    }
  }

}
