<?php
class SimplePodcastConstants {
  /**
   * Retrieve list of Extra Field names.
   *
   * PHP does not support arrays as constants until PHP 5.6
   * Using a function that returns an array in lieu of constants.
   *
   * @return array
   *
   */
  public static function getExtraFieldNames() {
    return array(
      self::EXTRA_FIELDS_FILE_DURATION,
      self::EXTRA_FIELDS_FILE_MIMETYPE,
      self::EXTRA_FIELDS_FILE_SIZE,
      self::EXTRA_FIELDS_SUBSCRIBE,
    );
  }
  /**
   * Retrieve Extra Fields array for podcast items.
   *
   * PHP does not support arrays as constants until PHP 5.6
   * Using a function that returns an array in lieu of constants.
   *
   * @return array
   *
   */
  public static function extra_fields_array_podcast_item() {
    return array(
      'display' => array(
        self::EXTRA_FIELDS_FILE_DURATION => array(
          'label' => t('Podcast Duration'),
          'description' => t('The duration of the podcast in the format of HH:MM:SS'),
          'weight' => 50, // Default initial weight
        ),
        self::EXTRA_FIELDS_FILE_MIMETYPE => array(
          'label' => t('Podcast Mime Type'),
          'description' => t('The mime type of the podcast.'),
          'weight' => 51, // Default initial weight
        ),
        self::EXTRA_FIELDS_FILE_SIZE => array(
          'label' => t('Podcast File Size in Bytes'),
          'description' => t('The file size of the podcast in bytes.'),
          'weight' => 52, // Default initial weight
        ),
        self::EXTRA_FIELDS_BODY => array(
          'label' => t('Podcast Rendered Body'),
          'description' => t('Podcast custom rendered content body.'),
          'weight' => 0, // Default initial weight
        ),
        self::EXTRA_FIELDS_SUBSCRIBE => array(
          'label' => t('Subscription Links'),
          'description' => t('Subscription links inherited from the parent Podcast Feed node.'),
          'weight' => 5, // Default initial weight
        ),
      ),
    );
  }

  public static function extra_fields_array_podcast_config() {
    return array(
      'display' => array(
        self::EXTRA_FIELDS_BODY => array(
          'label' => t('Podcast Rendered Body'),
          'description' => t('Podcast custom rendered content body.'),
          'weight' => 0, // Default initial weight
        ),
      ),
    );
  }

  public static function getItemFieldNames() {
    return array(
      self::FIELDS_ITEM_AUTHOR,
      self::FIELDS_ITEM_FEED,
      self::FIELDS_ITEM_COVER_ART,
      self::FIELDS_ITEM_DESCRIPTION,
      self::FIELDS_ITEM_EXPLICIT,
      self::FIELDS_ITEM_FILE,
      self::FIELDS_ITEM_SUMMARY,
      self::FIELDS_ITEM_TITLE,
      self::FIELDS_ITEM_TRANSCRIPT,
      self::EXTRA_FIELDS_FILE_DURATION,
      self::EXTRA_FIELDS_FILE_MIMETYPE,
      self::EXTRA_FIELDS_FILE_SIZE,
      self::FIELDS_ITEM_COPYRIGHT,
    );
  }
  public static function getConfigFieldNames() {
    return array(
      self::FIELDS_CONFIG_AUTHOR,
      self::FIELDS_CONFIG_COVER_ART,
      self::FIELDS_CONFIG_TOPIC,
      self::FIELDS_CONFIG_TITLE,
      self::FIELDS_CONFIG_SUMMARY,
      self::FIELDS_CONFIG_EXPLICIT,
      self::FIELDS_CONFIG_DESCRIPTION,
      self::FIELDS_CONFIG_OWNER,
      self::FIELDS_CONFIG_COPYRIGHT,
    );
  }

  // Extra Field Names
  const EXTRA_FIELDS_FILE_DURATION = 'simple_podcast_file_duration';
  const EXTRA_FIELDS_FILE_MIMETYPE = 'simple_podcast_file_mime_type';
  const EXTRA_FIELDS_FILE_SIZE = 'simple_podcast_file_size';
  const EXTRA_FIELDS_BODY = 'simple_podcast_content';
  const EXTRA_FIELDS_SUBSCRIBE = 'simple_podcast_item_subs';

  // Item Field Names
  const FIELDS_ITEM_COPYRIGHT = 'field_simple_podcast_copyright';
  const FIELDS_ITEM_AUTHOR = 'field_simple_podcast_author';
  const FIELDS_ITEM_COVER_ART = 'field_simple_podcast_cover_art';
  const FIELDS_ITEM_FEED = 'field_simple_podcast_feed';
  const FIELDS_ITEM_TITLE = 'title';
  const FIELDS_ITEM_SUMMARY = 'field_simple_podcast_summary';
  const FIELDS_ITEM_DESCRIPTION = 'field_simple_podcast_description';
  const FIELDS_ITEM_FILE = 'field_simple_podcast_file';
  const FIELDS_ITEM_EXPLICIT = 'field_simple_podcast_explicit';
  const FIELDS_ITEM_TRANSCRIPT = 'field_simple_podcast_transcript';
  const FIELDS_ITEM_SUBSCRIBE = 'field_simple_podcast_subscribe';

  // Feed Field Names
  const FIELDS_CONFIG_AUTHOR = self::FIELDS_ITEM_AUTHOR;
  const FIELDS_CONFIG_COVER_ART = self::FIELDS_ITEM_COVER_ART;
  const FIELDS_CONFIG_TOPIC = 'field_simple_podcast_topic';
  const FIELDS_CONFIG_TITLE = self::FIELDS_ITEM_TITLE;
  const FIELDS_CONFIG_SUMMARY = self::FIELDS_ITEM_SUMMARY;
  const FIELDS_CONFIG_EXPLICIT = self::FIELDS_ITEM_EXPLICIT;
  const FIELDS_CONFIG_DESCRIPTION = self::FIELDS_ITEM_DESCRIPTION;
  const FIELDS_CONFIG_OWNER = 'field_simple_podcast_owner';
  const FIELDS_CONFIG_COPYRIGHT = self::FIELDS_ITEM_COPYRIGHT;
  const FIELDS_CONFIG_SUBSCRIBE = self::FIELDS_ITEM_SUBSCRIBE;

  // Control constants
  const EXTRA_FIELDS_DO_NOT_RENDER = 'do_not_render';
  const QUERY_TAG_CHILDREN = 'SIMPLE_PODCAST_CHILDREN_QUERY_TAG';

  // Array Key constants:
  const ARRAY_KEY_RENDER_USE_DRUPAL_API = 'render_use_drupal_api';

  // Bundle name constants
  const BUNDLE_ITEM = 'simple_podcast_item';
  const BUNDLE_CONFIG = 'simple_podcast_feed_config';
}
