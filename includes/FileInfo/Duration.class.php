<?php
class SimplePodcastDuration {
  public $h;
  public $m;
  public $s;
  public $formatted;

  /**
   * @param $time
   *  Time as a string formatted in HH:MM:SS or MM:SS, or just seconds.
   * @param int $m
   *  If $time is seconds, $m will be included as minutes.
   * @param int $h
   *  If $time is seconds, $h will be included as hours.
   */
  public function __construct($time, $m=0, $h=0) {

    // If time is a string value, break it into hours, minutes, seconds.
    if (strpos((string)$time,':')) {
      // Convert id3 playtime string value into minutes and seconds
      $hms = explode(':', $time);
      $this->s = array_pop($hms) ?: 0;
      $this->m = array_pop($hms) ?: 0;
      // While most formats won't include hours, handle the possibility.
      $this->h = array_pop($hms) ?: 0;
    }
    // Time is in seconds
    else {
      // Ensure seconds is less than 60, and add any overages into minutes
      $this->s = (int)$time;
      if ($this->s > 60) {
        $this->m += (int) ($this->s / 60);
        $this->s = $m % 60;
      }
    }

    // Most formats don't include hours, and if the value was passed only as
    // seconds, while converting those to minutes and seconds, the minutes
    // calulated may exceed 60.

    // If minutes is greater than 60, recalculate hours.
    if ($this->m > 60) {
      $this->h += (int) ($this->m / 60);
      $this->m = $this->h % 60;
    }

    $this->formatted = sprintf("%02d:%02d:%02d", $this->h, $this->m, $this->s);
  }
}
