<?php
class SimplePodcastFileInfo {
  /**
   * @var array
   *   Errors while attempting to load the File Info will be stored here.
   */
  public $errors;

  /**
   * @var bool
   *   A variable indicating if the file was found and processed without error.
   */
  public $loaded;
  /**
   * @var string
   *   Full external path to item
   */
  public $url;

  /**
   * @var int
   *   File size in bytes.
   */
  public $filesize;

  /**
   * @var string
   *   Mimetype of the file.
   */
  public $mimetype;

  /**
   * @var SimplePodcastDuration
   */
  public $duration;

  /**
   * @param $file_info
   *   Various file information from a Drupal 7 File field.
   *
   */
  public function __construct($file_info) {
    $this->errors = array();
    $this->loaded = false;
    $this->mimetype = isset($file_info['filemime']) ? $file_info['filemime'] : '';
    $file_uri = $file_info['uri'];
    $file_size = $file_info['filesize'];
    $playtime_string = 0;

    if (empty($file_uri)) {
      $this->errors[] = 'Error, $file not instance of a File in SimplePodcastFileInfo.';
      return;
    }

    // Verify file exists prior to attempting id3 processing, as external files
    // are saved as managed files
    // Ex: this prevents errors on media_youtube or media_vimeo entries.
    $file_realpath = drupal_realpath($file_uri);
    if (!file_exists($file_realpath)) {
      $this->errors[] = 'File not found at ' . $file_realpath . ' for file uri ' . $file_uri;
    }
    else {
      // Attempt to build a getid3 instance for processing.
      $getid3 = getid3_instance();
      if ($getid3 == NULL) {
        $this->errors[] = 'Unable to load id3 library.';
      }
      else {

        // Store original values (a single instance is used for the entire platform).
        $opt_tag_id3v1 = $getid3->option_tag_id3v1;
        $opt_tag_id3v2 = $getid3->option_tag_id3v2;
        $opt_tag_lyrics3 = $getid3->option_tag_lyrics3;
        $opt_tag_apetag = $getid3->option_tag_apetag;

        // Skip analyzing information we don't need.
        $getid3->option_tag_id3v1 = FALSE;
        $getid3->option_tag_id3v2 = FALSE;
        $getid3->option_tag_lyrics3 = FALSE;
        $getid3->option_tag_apetag = FALSE;

        // If file info cannot be retrieved, stop processing for this field and
        // return the default.
        $id3_info = $getid3->analyze($file_realpath);
        if (!$id3_info) {
          $this->errors[] = 'id3 cannot find file ' . $file_realpath;
        }
        elseif (isset($id3_info['error']) && is_array($id3_info['error'])) {
          // Add error, but don't return, we need to reset variables first.
          $this->errors += $id3_info['error'];
        }
        else {
          // Replace our file info with fresher id3 info.
          if (!empty($id3_info['mime_type'])) {
            $this->mimetype = $id3_info['mime_type'];
          }
          if (!empty($id3_info['filesize'])) {
            $file_size = $id3_info['filesize'];
          }
          if (!empty($id3_info['playtime_string'])) {
            $playtime_string = $id3_info['playtime_string'];
          }
        }
        // Restore settings (a single instance is used for the entire platform).
        $getid3->option_tag_id3v1 = $opt_tag_id3v1;
        $getid3->option_tag_id3v2 = $opt_tag_id3v2;
        $getid3->option_tag_lyrics3 = $opt_tag_lyrics3;
        $getid3->option_tag_apetag = $opt_tag_apetag;
      }
    }

    // Build return data object from gathered data.

    $this->url = file_create_url($file_uri);

    //TODO: create class for this
    $filesize_object = new stdClass();
    $filesize_object->data = $file_size;
    // Convert from bytes to MB with two decimal precision.
    $filesize_object->formatted = sprintf("%.2f", $filesize_object->data / (1024 * 1024)) . ' MB';
    $this->filesize = $filesize_object;

    $this->duration = new SimplePodcastDuration($playtime_string);

    $this->loaded = true;
  }

}
