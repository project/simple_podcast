# Customization Tips
The Simple Podcast module attempts to introduce as few configuration changes as is necessary to support the behavior of this module.

## Creating a Custom Rendering Format
Create a custom renderer:

    class CustomModuleSimplePodcastHtmlRenderer extends SimplePodcastHtmlRenderer {

      // If you want to override the list behavior, include this function and set the new path.
      public function getListTemplateFilePath (){
        return '/full/absolute/path/to/my/list.tpl.php';
      }

      // if you want to point to a different item template, include this function and set the new path.
      public function getItemTemplateFilePath() {
        return '/full/absolute/path/to/my/item.tpl.php';
      }

    }

Define the following hook in a custom module:

    /**
     * Implementation of hook_simple_podcast_plugins().
     */
    custom_module_simple_podcast_plugins() {
      return array(
        new SimplePodcastPluginInfo('myformat', MySimplePodcastRenderer::class)
      );
    }

NOTE: In the example above, MySimplePodcastRenderer must extend the SimplePodcastRenderer class or a class that extends it, and 'myformat' is the format query parameter value that this renderer should respond to.

## Default HTML rendering override
Create a custom rendering format as described above.  Set 'myformat' to 'default'.

## XML rendering override
Create a custom rendering format as described above.  Set 'myformat' to 'xml'.

## iTunes rendering override
Create a custom rendering format as described above.  Set 'myformat' to 'itunes'.



## TODO:
create pathauto settings for location of podcast items
create pathauto settings for location of config items
export bundles, pathauto settings, and taxonomy vocab/terms into feature 
  exclude OG field data, put that in a separate feature for energy_podcast module to be based off of.
update energy_podcast
  alter simple_podcast behavior to add OG group filtering to the filtered results by subclassing the plugins and altering the item list rendering logic
  Update renderer so that it can get the children items based on topics passed in
    in the energy_podcast module, override this function to filter by OG as well.

