<?php
class SimplePodcastITunesRenderer extends SimplePodcastRenderer {
  const MAX_STANDARD_LENGTH = 255;
  const MAX_SUMMARY_LENGTH = 4000;

  public function getHeaders(array &$headers) {
    $headers = array(
      'X-Content-Type-Options' => 'nosniff',
      'Content-type' => 'application/rss+xml charset=utf-8',
    );

    return true;
  }

  public function getListTemplateFilePath() {
    return __DIR__ . '/templates/itunes/list.tpl.php';
  }

  public function getItemTemplateFilePath() {
    return __DIR__ . '/templates/itunes/item.tpl.php';
  }

  public function ensureItemPublishedVariable(&$vars, &$node) {
    if (parent::ensureItemPublishedVariable($vars, $node)) {
      $d = date_default_timezone_get();
      date_default_timezone_set('UTC');
      $vars['published'] = date('D, d M Y H:i:s O', $vars['published']);
      date_default_timezone_set($d);
      return true;
    }
    return false;
  }

  public function ensureConfigSummaryVariable(&$vars, &$node) {
    if (parent::ensureConfigSummaryVariable($vars, $node)) {
      // Feeds use the "summary" field to fill their "subtitle" XML tag.
      $vars['summary'] = $this->itunes_clean_tags($vars['summary']);
      return true;
    }
    return false;
  }

  public function ensureConfigDescriptionVariable(&$vars, &$node) {
    if (parent::ensureConfigDescriptionVariable($vars, $node)) {
      // Feeds use the "description" field to fill their "summary" XML tag.
      $vars['description'] = $this->itunes_clean_tags($vars['description'], static::MAX_SUMMARY_LENGTH);
      return true;
    }
    return false;
  }


  public function ensureItemSummaryVariable(&$vars, &$node) {
    if (parent::ensureItemSummaryVariable($vars, $node)) {
      // Items use the "summary" field to fill their "subtitle" XML tag.
      $vars['summary'] = $this->itunes_clean_tags($vars['summary']);
      return true;
    }
    return false;
  }

  public function ensureItemDescriptionVariable(&$vars, &$node) {
    if (parent::ensureItemDescriptionVariable($vars, $node)) {
      // Items use the "description" field to fill their "summary" XML tag.
      $vars['description'] = $this->itunes_clean_tags($vars['description'], static::MAX_SUMMARY_LENGTH);
      return true;
    }
    return false;
  }

  public function render(array &$variables = array()) {
    if (arg(0) === 'node' && arg(2) !== 'edit') {
      if (parent::render($variables) === 'NULL' || !parent::loadVariables($variables)) {
        // If the default render fails or the default loadVariables method returns FALSE,
        // there was some error on inheriting required iTunes properties.
        // We should fail out to a 404 instead of allowing corrupt data to display.
        drupal_not_found();
      }
    }
    return TRUE;
  }

  /**
   * Cleans out all tags apart from anchor tags, which get wrapped in
   * <![CDATA[ and ]], as defined by iTunes Podcast documentation.
   * https://help.apple.com/itc/podcasts_connect/#/itcb54353390 . Does this while
   * maintaining the set character limit, defaulting to the character limit
   * defined in the same docs.
   *
   * @todo: revisit and see if rewriting with DOMDocument makes this easier or
   * more flexible.
   *
   * @param string $string
   *   String to strip and cut down in length.
   * @param int $character_limit
   *   If left empty will be set to the standard length for podcast fields.
   * @return string
   */
  protected function itunes_clean_tags($string, $character_limit = NULL) {
    $anchor_regex = '/(<a.*\/a>)/';
    $orphaned_anchor_regex = '/(<a(?!.*<\/a>).*>)/';
    $broken_anchor_regex = '/(<a(?!.*<\/a>).*)/';
    $anchor_prefix = '<![CDATA[';
    $anchor_suffix = ']]>';
    $anchor_wrap_length = strlen($anchor_prefix . $anchor_suffix);

    if ($character_limit === NULL) {
      $character_limit = static::MAX_STANDARD_LENGTH;
    }

    // First strip all tags but anchor tags.
    $string = strip_tags($string, '<a>');
    // Then reduce to our max character limit.
    $string = substr($string, 0, $character_limit);
    // We want to wrap all anchor tags in <![CDATA[ ]]>, but that may put us back
    // over the character limit. So find how many anchor tags there are and subtract
    // the wrap_length from our character limit, to ensure we have enough space.

    if ($matches = preg_match_all($anchor_regex, $string)) {
      // Reduce our character limit by the number of full anchor matches.
      $character_limit = $character_limit - (count($matches) * $anchor_wrap_length);
    }
    // Cut the string down again; it should now match the character limit without
    // the CDATA tags.
    $string = substr($string, 0, $character_limit);
    // There may be orphaned or broken anchor tags, so clean those up.
    $string = preg_replace(array($orphaned_anchor_regex, $broken_anchor_regex), '', $string);
    // Finish by finally inserting the CDATA wrappers.
    $string = preg_replace($anchor_regex, $anchor_prefix . '$1' . $anchor_suffix, $string);

    return $string;
  }
}
