<?php
use SimplePodcastConstants as C;

class SimplePodcastHtmlRenderer extends SimplePodcastRenderer {
  const USE_DRUPAL_RENDER_API = true;
  protected $cssFiles;

  public function __construct($node, $language=LANGUAGE_NONE, $format='default') {
    parent::__construct($node, $language, $format);

    // Build the array of CSS files for later reference.
    $this->cssFiles = array_merge($this->getItemCssFilePaths(), $this->getListCssFilePaths());
    foreach ($this->cssFiles as $cssFile) {
      drupal_add_css($cssFile, array('group' => CSS_DEFAULT, 'type' => 'file'));
    }
  }

  public function getHeaders(array &$headers) {
    // Don't set headers for HTML output.  Allow Drupal to handle this.
    return FALSE;
  }

  public function render(array &$variables = array()) {
    if(!(arg(0) == 'node' && arg(2) == 'edit')) {
      $variables['node']->content[C::EXTRA_FIELDS_BODY] = parent::render($variables);
      // All other extra fields processed here.
      // Fields rendered via the simple_podcast renderer should already have markup added from
      // their respective ensure<fieldname>Variable functions. In the case that a custom plugin
      // uses Drupal's API, they still may not have the field set to be visible, so we need to
      // manually be sure prior to rendering it.
      $extra_fields_display = field_extra_fields_get_display('node', $variables['node']->type, 'default');

      if (isset($extra_fields_display[C::EXTRA_FIELDS_SUBSCRIBE]) && $extra_fields_display[C::EXTRA_FIELDS_SUBSCRIBE]['visible'] === TRUE) {
        $variables['node']->content[C::EXTRA_FIELDS_SUBSCRIBE] = array(
          '#markup' => theme_item_list(array(
            'items' => $variables['subscribe'],
            'type' => 'ul',
            'attributes' => array(
              'class' => 'field-name-field-simple-podcast-subscription-links',
            ),
          )),
        );
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function getListTemplateFilePath () {
    return __DIR__ . '/templates/html/list.tpl.php';
  }

  public function getItemTemplateFilePath() {
    return __DIR__ . '/templates/html/item.tpl.php';
  }

  /**
   * Get the locations of the plugin's listing css files.
   *
   * @return array
   *   The locations of the plugin's listing css files.
   */
  public function getItemCssFilePaths() {
    $files = array();
    $files[] = drupal_get_path('module', 'simple_podcast') . '/plugins/simplepodcast/formatters/templates/html/css/item.css';
    return $files;
  }

  /**
   * Get the locations of the plugin's item css files.
   *
   * @return array
   *   The location sof the plugin's item css files.
   */
  public function getListCssFilePaths() {
    $files = array();
    $files[] = drupal_get_path('module', 'simple_podcast') . '/plugins/simplepodcast/formatters/templates/html/css/list.css';
    return $files;
  }

  public function formatImage($image_uri, $image_alt, $style) {

    $settings = array(
      'style_name' => $style,
      'path' => $image_uri,
      'alt' => $image_alt,
      'title' => NULL,
      'attributes' => array(),
    );

    return theme_image_style($settings);
  }

  public function ensureItemPublishedVariable(&$vars, &$node) {
    if (parent::ensureItemPublishedVariable($vars, $node)){
      $vars['published'] = date('n/d/Y H:i:s T', $vars['published']);
      return (!empty($vars['published']));
    }
    return false;
  }
  public function ensureConfigImageVariables(&$vars, &$node) {
    if (parent::ensureConfigImageVariables($vars, $node)){
      $vars['image'] = $this->formatImage($vars['image_uri'], $vars['image_alt'], 'simple_podcast_config_html_image');
      return !empty($vars['image']);
    }
    return false;
  }
  public function ensureItemImageVariables(&$vars, &$node) {
    if (parent::ensureItemImageVariables($vars, $node)){
      $vars['image'] = $this->formatImage($vars['image_uri'], $vars['image_alt'], 'simple_podcast_item_html_image');
      return !empty($vars['image']);
    }
    return false;
  }
}
