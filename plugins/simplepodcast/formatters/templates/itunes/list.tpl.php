<?php print '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
  <channel>
    <title><?php print $title; ?></title>
    <link><?php print $link; ?></link>
    <language><?php print $language; ?></language>
<?php if (!empty($explicit)): ?>
    <itunes:explicit><?php print $explicit; ?></itunes:explicit>
<?php endif; ?>
    <copyright><?php print $copyright; ?></copyright>
    <itunes:author><?php print $author; ?></itunes:author>
    <itunes:subtitle><?php print $summary; ?></itunes:subtitle>
    <itunes:summary><?php print $description; ?></itunes:summary>
    <description><?php print $description; ?></description>
    <itunes:owner>
      <itunes:name><?php print $owner['name']; ?></itunes:name>
      <itunes:email><?php print $owner['email']; ?></itunes:email>
    </itunes:owner>
    <itunes:image href="<?php print $image_url; ?>" /><?php
    $n = "\n";
    // If we have categories, iterate and render.
    if (!empty($categories) && is_array($categories)) {
      $delayed_output = '';

      // Array walk recursive, but include all nodes, not just leaf nodes.
      $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories), RecursiveIteratorIterator::SELF_FIRST);

      foreach ($iterator as $key => $children) {
        $indent = str_repeat('  ', $iterator->getDepth() + 2);
        print $n . $indent;
        if (!empty($children) && is_array($children)) {
          printf('<itunes:category text="%s">', $key);
          $delayed_output = sprintf('%s%s</itunes:category>%s', $n, $indent, $delayed_output);
        }
        else {
          printf('<itunes:category text="%s"/>', $key);
        }
      }
      print $delayed_output;
    }
    // Render children items, if any.
    $this->renderChildren($items);?>
  </channel>
</rss>
<?php // end of list xml template
