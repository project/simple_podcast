XML template files for rendering a podcast in an iTunes compatible format.

Expects the $variables variable to contain the following architecture:

'''
$variables = array(
  'title'       => 'HTML text',
  'link'        => 'uri path to podcast item page',
  'language'    => 'default language in the format of en-us',
  'copyright'   => 'copyright details',
  'summary'     => '255 character or less description of the podcast list',
  'author'      => 'name to give as author of the list, e.g. Energy.gov',
  'description' => 'detailed description of the podcast list, HTML is allowed',
  'owner'       => array(
    'name'  => 'name of who owns the content, e.g. Energy.gov, EERE',
    'email' => 'email-of-the-owner@domain.com'
  ),
  'image'       => array(
    'url' => 'http://domain.com/path-to-podcast-feed-cover-art-file.png',
  ),
  'categories'  => array(
    'Category1 Text Name' => FALSE, 
    'Category2 Text Name'  => array (
      'Child Category Text Name 1',
      'Child Cagegory Text Name 2'
    ),
  ),
  'items'       => array(
    // NID
    '145153' => array(
      'title'       => 'Title of Podcast',
      'author'      => 'Author of podcast item, e.g. Energy.gov or EERE',
      'summary'     => '255 character or less description of the podcast list',
      'description' => 'detailed description of the podcast list, HTML is allowed',
      'image'       => array(
        'url' => 'http://domain.com/path-to-podcast-cover-art-file.png',
      ),
      'file'          => array(
        'url'         => 'http://domain.com/files/podcasts/2016/03/28/ThePodcastFile.mp3',
        // file size in bytes
        'bytes'       => 8727310,
        // audio format, e.g. audio/mpg or audio/x-m4a
        'type'        => 'audio/mpg',
        'guid'        => 'http://domain.com/files/podcasts/2016/03/28/ThePodcastFile.mp3',
        // published date
        'published'   => 'Mon, 28 Mar 2016 19:00:00 GMT',
        // optional duration HH:MM:SS
        'duration'    => '00:07:04'
    ),
    '121515' => array(
      'title'       => 'Title of another Podcast',
      'author'      => 'Author of this podcast item, e.g. Energy.gov or EERE',
      'summary'     => '255 character or less description of the podcast list',
      'description' => 'detailed description of the podcast list, HTML is allowed',
      'image'       => array(
        'url' => 'http://domain.com/path-to-podcast-cover-art-file.png',
      ),
      'file'          => array(
        'url'         => 'http://domain.com/files/podcasts/2016/03/27/AnotherPodcastFile.mp3',
        // file size in bytes
        'bytes'       => 8923310,
        // audio format, e.g. audio/mpg or audio/x-m4a
        'mimetype'    => 'audio/mpg',
        'guid'        => 'http://domain.com/files/podcasts/2016/03/27/AnotherPodcastFile.mp3',
        // published date
        'published'   => 'Sun, 27 Mar 2016 18:00:00 GMT',
        // optional duration HH:MM:SS
        'duration'    => '00:07:04'
      )
    )
  )
);
'''
