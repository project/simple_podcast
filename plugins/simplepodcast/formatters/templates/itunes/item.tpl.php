
    <item>
      <title><?php print $item_title; ?></title>
      <itunes:author><?php print $item_author; ?></itunes:author>
      <itunes:subtitle><?php print $item_summary; ?></itunes:subtitle>
      <itunes:summary><?php print $item_description; ?></itunes:summary>
      <itunes:image href="<?php print $item_image_url; ?>" />
      <enclosure url="<?php print $item_file['url']; ?>" length="<?php print $item_file['bytes']; ?>" type="<?php print $item_file['mimetype']; ?>" />
      <guid isPermaLink="false"><?php print $item_guid; ?></guid>
      <pubDate><?php print $item_published; ?></pubDate>
      <itunes:duration><?php print $item_file['duration']; ?></itunes:duration>
<?php if (!empty($explicit)): ?>
      <itunes:explicit><?php print $explicit; ?></itunes:explicit><?php endif; ?>
    </item><?php
