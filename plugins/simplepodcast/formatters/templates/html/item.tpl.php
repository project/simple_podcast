<?php if ($item_only): $i = 1; if (empty($h1)): $h1 = 1; endif; if (empty($h2)): $h2 = 2; endif; if (empty($h3)): $h3 = 3; endif; ?>
  <div class="<?php print join(' ', $item_classes); ?>">
<?php else: ?>
  <li class="<?php print join(' ', $item_classes); ?>">
<?php endif; ?>
    <article id="podcast-item-<?php print $i; ?>">
      <a href="#podcast-item-<?php print $i; ?>-file" class="element-invisible"><?php print t('Jump to Podcast File.'); ?></a>
      <header>
        <dl class="podcast-item-meta-data">
          <dt><?php print t('Podcast Meta Data'); ?></dt>
          <dd class="podcast-item-cover-art">
            <figure>
              <?php print $item_image; ?>
              <figcaption class="element-invisible">
                <?php print $item_title . ' ' . t('Album Art'); ?>
              </figcaption>
            </figure>
          </dd><?php if ($item_only && !empty($item_categories) && is_array($item_categories)): // If we have categories, iterate and render. ?>
          <dd class="podcast-item-categories">
            <dl>
              <dt><?php print t('Categories'); ?></dt>
              <?php foreach ($item_categories as $link => $classes): ?>
                <dd class="<?php print $classes; ?>"><?php print $link; ?></dd>
              <?php endforeach; ?>
            </dl>
          </dd><?php endif; ?>
          <dd class="podcast-item-title<?php if ($item_only): print ' element-invisible'; endif; ?>">
            <h<?php print $h1; ?>>
              <?php if (!$item_only): ?>
                <a href="<?php print $item_path; ?>">
              <?php endif; ?>
              <?php print $item_title ?>
              <?php if (!$item_only): ?>
                </a>
              <?php endif; ?>
            </h<?php print $h1; ?>>
          </dd>
          <dd class="podcast-item-author">
            <dl>
              <dt><?php print t('Author'); ?></dt>
              <dd><?php print $item_author; ?></dd>
            </dl>
          </dd>
          <dd class="podcast-subscribe">
            <dl>
              <dt><?php print t('Subscribe'); ?></dt>
              <?php foreach ($subscribe as $subscribe_link): ?>
                <dd><?php print($subscribe_link); ?></dd>
              <?php endforeach; ?>
            </dl>
          </dd>
          <dd class="podcast-copyright">
            <dl>
              <dt><?php print t('Copyright'); ?></dt>
              <dd><?php print $item_copyright; ?></dd>
            </dl>
          </dd>
          <dd class="podcast-item-subtitle">
            <h<?php print $h2; ?>>
              <?php print $item_summary ?>
            </h<?php print $h2; ?>>
          </dd>
<?php if (!empty($explicit)): ?>
          <dd>
            <dl>
              <dt>Content Rating</dt>
  <?php if ($explicit == 'clean'): ?>
              <dd>Clean</dd>
  <?php elseif ($explicit == 'yes'): ?>
              <dd>Explicit</dd>
  <?php else: //impossible to reach, but just in case ?>
              <dd>None</dd>
  <?php endif; ?>
            </dl>
          </dd>
<?php endif; ?>
        </dl>
      </header>
      <section class="podcast-item-file" id="podcast-item-<?php print $i; ?>-file">
<?php if ($item_file['type'] == 'video'): ?>
        <video controls class="podcast-item-<?php print $i; ?>-file-item" aria-describedby="podcast-item-<?php print $i; ?>-description"<?php if (!empty($item_file['thumb'])): ?> poster="<?php print $item_file['thumb']; ?>"<?php endif;?> tabindex="0" title="<?php print $item_title; ?>">
          <source src="<?php print $item_file['url']; ?>" type="<?php print $item_file['mimetype']; ?>" />
          <source src="#podcast-item-transcript-<?php print $i; ?>">
          <a class="podcast-item-file-item" href="<?php print $item_file['url']; ?>" aria-describedby="podcast-item-<?php print $i; ?>-description"><?php print t("Play video"); ?></a>
        </video>
<?php elseif ($item_file['type'] == 'audio'): ?>
        <audio controls class="podcast-item-file-item" aria-describedby="podcast-item-<?php print $i; ?>-description">
          <source src="<?php print $item_file['url']; ?>" type="<?php print $item_file['mimetype']; ?>" />
          <source src="#podcast-item-transcript-<?php print $i; ?>">
          <a class="podcast-item-file-item" href="<?php print $item_file['url']; ?>" aria-describedby="podcast-item-<?php print $i; ?>-description"><?php print t("Play audio"); ?></a>
        </audio>
<?php endif; ?>
        <a class="podcast-item-file-download" download="download" href="<?php print $item_file['url']; ?>" aria-describedby="podcast-item-<?php print $i; ?>-description"><?php print t("Download File"); ?></a>
      <?php if (!empty($item_transcript) && !$item_only): ?>
        <a class="podcast-item-transcript" href="<?php print $item_path . '#podcast-item-transcript'; ?>"><span class="element-invisible"><?php print t('Click to view full content item to read the'); ?></span><?php print t('Transcript'); ?></a>
      <?php endif; ?>
      </section>
      <section id="podcast-item-<?php print $i; ?>-description" class="podcast-item-description<?php if (!$item_only): print ' element-invisible'; endif; ?>">
        <h<?php print $h3; ?>>
          <?php print t('Description'); ?>
        </h<?php print $h3; ?>>
        <?php print $item_description; ?>
      </section>
<?php if (!empty($item_transcript) && $item_only): ?>
      <section class="podcast-item-transcript" id="podcast-item-transcript">
        <h<?php print $h3; ?>>
          <?php print t('Transcript'); ?>
        </h<?php print $h3; ?>>
        <div class="podcast-item-transcript-text">
          <?php print $item_transcript; ?>
        </div>
      </section>
<?php endif; ?>
      <footer class="element-invisible">
        <a href="#podcast-item-<?php print $i; ?>-file"><?php print t('Jump to Podcast File.'); ?></a>
        <a href="#podcast-item-<?php print $i; ?>"><?php print t('Jump to Podcast Meta Data.'); ?></a>
        <a id="podcast-item-<?php print $i; ?>-end"><?php print t('End of podcast item data.'); ?></a>
      </footer>
    </article>
<?php if (!$item_only): ?>
  </li>
<?php else: ?>
  </div>
<?php endif;

