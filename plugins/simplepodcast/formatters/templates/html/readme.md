HTML template files for rendering a podcast list and items on an HTML page.

Expects the $variables variable to contain the same fields as the itunes formatter, with one addition:

The items array contains elements keyed by NID, and each item must have a 'trascript' field such as:
$variables['items']['1513541']['transcript'] = 'an html body of content that is a text equivalent of the audio for this podcast item';

This template assumes you have all fields hidden via the UI, and are letting the field be rendered here instead.

//TODO: integrate with the Drupal view modes to get whether a field is shown or hidden and show or hide the template's element rendering.

