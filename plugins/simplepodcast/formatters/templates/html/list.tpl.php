<article id="podcast-list" class="<?php print implode(' ', $classes); ?>">
  <a href="#podcast-children" class="element-invisible">Jump to Podcast Items List.</a>
  <header>
    <?php if (!empty($subscribe['iTunes']) || !empty($subscribe['RSS'])): ?>
    <div class="podcast-subscribe">
      <?php if (!empty($subscribe['iTunes'])): ?><a class="podcast-itunes-subscribe" href="<?php print $subscribe['iTunes']; ?>"><?php print t('Subscribe via iTunes'); ?></a><?php endif; ?>
      <?php if (!empty($subscribe['RSS'])): ?><a class="podcast-rss-subscribe" href="<?php print $subscribe['RSS'] ?>"><?php print t('Subscribe via RSS'); ?></a><?php endif; ?>
    </div>
    <?php endif; ?>
    <dl class="podcast-meta-data">
      <dt><?php print t('Podcast Meta Data'); ?></dt>
      <dd class="podcast-cover-art">
        <figure>
          <?php print $image; ?>
          <figcaption>
            <?php print $title . ' ' . t('Album Art'); ?>
          </figcaption>
        </figure>
      </dd><?php
      // If we have categories, iterate and render.
      $n = "\n"; if (!empty($categories) && is_array($categories)): ?>
      <dd class="podcast-categories">
        <dl><?php
          $delayed_output = '';
          $title = t('Category');
          // Array walk recursive, but include all nodes, not just leaf nodes.
          $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($categories), RecursiveIteratorIterator::SELF_FIRST);

          $d = 5;
          foreach ($iterator as $key => $children) {
            $indent = str_repeat('  ', $iterator->getDepth() + $d);
            print $n . $indent;
            print "<dt>{$title}</dt>";
            if (!empty($children) && is_array($children)) {
              $title[0] = strtolower($title[0]);
              $title = t('Sub-' . $title);
              print "{$n}{$indent}<dd><span>{$key}</span>";
              print "{$n}{$indent}  <dl>";
              $d += 2;
              $after = "{$n}{$indent}  </dl>";
              $after .= "{$n}{$indent}</dd>";
              $delayed_output = $after . $delayed_output;
            }
            else {
              print "{$n}{$indent}<dd>{$key}</dd>";
            }
          }
          print $delayed_output; ?>
        </dl>
      </dd><?php endif; ?>
      <dd class="podcast-title element-invisible">
        <h<?php print $h1; ?>>
          <?php print $title ?>
        </h<?php print $h1; ?>>
      </dd>
      <dd class="podcast-author">
        <dl>
          <dt><?php print t('Author'); ?></dt>
          <dd><?php print $author; ?></dd>
        </dl>
      </dd>
      <dd class="podcast-subscribe">
        <dl>
          <dt><?php print t('Subscribe'); ?></dt>
          <?php foreach ($subscribe as $subscribe_link): ?>
            <dd><?php print($subscribe_link); ?></dd>
          <?php endforeach; ?>
        </dl>
      </dd>
      <dd class="podcast-copyright">
        <dl>
          <dt><?php print t('Copyright'); ?></dt>
          <dd><?php print $copyright; ?></dd>
        </dl>
      </dd>
      <dd class="podcast-subtitle">
        <h<?php print $h2; ?>>
          <?php print $summary; ?>
        </h<?php print $h2; ?>>
      </dd>
      <dd class="podcast-owner">
        <dl>
          <dt><?php print t('Contact'); ?></dt>
          <dd class="podcast-owner-name">
            <dl>
              <dt><?php print t('Name'); ?></dt>
              <dd><a href="mailto:<?php print $owner['email']; ?>"><?php print $owner['name']; ?></a></dd>
            </dl>
          <dd class="podcast-owner-email">
            <dl>
              <dt><?php print t('Email'); ?></dt>
              <dd><?php print $owner['email']; ?></dd>
            </dl>
          </dd>
        </dl>
      </dd>
    </dl>
  </header>
  <section class="podcast-description" id="podcast-description">
    <h<?php print $h3; ?>>
      <?php print t('Description'); ?>
    </h<?php print $h3; ?>>
    <div class="podcast-description-text">
      <?php print $description; ?>
    </div>
  </section>
  <section class="podcast-children" id="podcast-children">
    <ol aria-describedby="#podcast-children-description">
      <?php $this->renderChildren($items); ?>
    </ol>
    <span class="element-invisible" id="podcast-children-description"><?php print t('A list of podcast items for this feed.'); ?><a href="#podcast-description"><?php t('Jump to a more detailed description.'); ?></a></span>
  </section>
  <footer class="element-invisible">
    <a href="#podcast-children"><?php print t('Jump to top of podcast item list.'); ?></a>
    <a href="#podcast-list"><?php print t('Jump to top of page content'); ?></a>
    <a href="#"><?php print t('Jump to top of page.'); ?></a>
  </footer>
</article>
<?php
