<?php
/**
 * @file Drush commands.
 */

/**
 * Implements hook_drush_cache_clear().
 *
 * @see drush_cache_clear_types()
 */
function simple_podcast_drush_cache_clear(&$types) {
  $types['simple-podcast'] = 'simple_podcast_invalidate_caches';
}

/**
 * Clears the Simple Podcast cache.
 *
 * @see drupal_flush_all_caches()
 */
function simple_podcast_invalidate_caches() {
  // Clear any and all Entity Caches
  SimplePodcastEntityCache::clearAll();
}
