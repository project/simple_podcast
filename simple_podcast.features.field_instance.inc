<?php
/**
 * @file
 * simple_podcast.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function simple_podcast_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-simple_podcast_feed_config-field_simple_podcast_subscribe'.
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_subscribe'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select from various automatically-generated subscribe links or add a custom link URL and label.

  "iTunes" will result in an ITPC feed, for podcasts without an iTunes feed ID. Use a custom link if you have an iTunes feed ID.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'field_collection',
        'settings' => array(
          'add' => 'Add',
          'delete' => 'Delete',
          'description' => TRUE,
          'edit' => 'Edit',
          'view_mode' => 'full',
        ),
        'type' => 'hidden',
        'weight' => 23,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'field_collection',
        'settings' => array(
          'add' => 'Add',
          'delete' => 'Delete',
          'description' => TRUE,
          'edit' => 'Edit',
          'view_mode' => 'full',
        ),
        'type' => 'hidden',
        'weight' => 23,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_subscribe',
    'label' => 'Subscribe Links',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'field_collection_embed',
      'weight' => 15,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_simple_podcast_subscribe-field_simple_podcast_sub_type'.
  $field_instances['field_collection_item-field_simple_podcast_subscribe-field_simple_podcast_sub_type'] = array(
    'bundle' => 'field_simple_podcast_subscribe',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_simple_podcast_sub_type',
    'label' => 'Subscription Link Type',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_select',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_simple_podcast_subscribe-field_simple_podcast_sub_link'.
  $field_instances['field_collection_item-field_simple_podcast_subscribe-field_simple_podcast_sub_link'] = array(
    'bundle' => 'field_simple_podcast_subscribe',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter a custom URL and label for custom field elements.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_simple_podcast_sub_link',
    'label' => 'Custom Link',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'link_field',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_author'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_author'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The name to show as the author of the overall feed.  Individual podcast items can have separate authors.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_author',
    'label' => 'Author',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 1,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_copyright'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_copyright'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Optional copyright information. Text from this field will be preceded with a &#169; symbol. If this field is empty, the copyright will be set to the current site\'s name.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 17,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_copyright',
    'label' => 'Copyright',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_cover_art'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_cover_art'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'deleted' => 0,
    'description' => 'Cover art image, with a minimum size of 1400 x 1400 pixels and a maximum size of 3000 x 3000 pixels, 72 dpi, in JPEG or PNG format with appropriate file extensions (.jpg, .png), and in the RGB colorspace.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_cover_art',
    'label' => 'Cover Art',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '5 MB',
      'max_resolution' => '3000x3000',
      'min_resolution' => '1400x1400',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 0,
          'image' => 'image',
          'pdf' => 0,
          'video' => 0,
        ),
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_description'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_description'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Full text description of the Podcast feed.  Allows html tags, but for highest compatibility with podcast clients, it is best to stick to &lt;a&gt;, &lt;em&gt;, or &lt;strong&gt; tags.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_description',
    'label' => 'Description',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => '',
        'maxlength_js_enforce' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'maxlength_js_truncate_html' => 0,
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_explicit'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_explicit'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => array(
      0 => array(
        'value' => 'no',
      ),
    ),
    'deleted' => 0,
    'description' => 'The most common value for this field is "No."  If you are unsure, leave the selected value set to "No."

Select "No" to in indicate that the podcast does not require an explicit language or adult content parental advisory notice, but also has not been certified as having absolutely no material that may be considered explicit or adult in nature to some recipients.
Select "Yes" if you wish to warn recipients of your podcast of the presence of explicit language or adult content.
Select "Clean" if you wish to certify that your podcast has absolutely no explicit language or adult content.
',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_explicit',
    'label' => 'Explicit',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_buttons',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_owner'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_owner'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => NULL,
    'default_value_function' => '',
    'deleted' => 0,
    'description' => 'Administrative contact for the podcast feed. ',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_owner',
    'label' => 'Owner',
    'required' => 1,
    'settings' => array(
      'behaviors' => array(
        'prepopulate' => array(
          'status' => 0,
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'STARTS_WITH',
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_summary'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_summary'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A 255 character or less description of the podcast item.  Some HTML is allowed, but it is best to limit it to &lt;a&gt;, &lt;em&gt;, or &lt;strong&gt; tags, for best compatibility with podcast clients.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_summary',
    'label' => 'Summary',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => '',
        'maxlength_js_enforce' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'maxlength_js_truncate_html' => 0,
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_feed_config-field_simple_podcast_topic'
  $field_instances['node-simple_podcast_feed_config-field_simple_podcast_topic'] = array(
    'bundle' => 'simple_podcast_feed_config',
    'default_value' => array(),
    'deleted' => 0,
    'description' => 'Select the topic that best identifies the content associated with this feed.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_topic',
    'label' => 'Topic',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'hs_taxonomy',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'taxonomy_hs',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_item-field_simple_podcast_author'
  $field_instances['node-simple_podcast_item-field_simple_podcast_author'] = array(
    'bundle' => 'simple_podcast_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_author',
    'label' => 'Author',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 1,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_item-field_simple_podcast_cover_art'
  $field_instances['node-simple_podcast_item-field_simple_podcast_cover_art'] = array(
    'bundle' => 'simple_podcast_item',
    'deleted' => 0,
    'description' => 'Cover art must be in the JPEG or PNG file formats and in the RGB color space with a minimum size of 1400 x 1400 pixels and a maximum size of 3000 x 3000 pixels.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_cover_art',
    'label' => 'Cover Art',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'podcasts/cover_art',
      'file_extensions' => 'png jpg jpeg',
      'max_filesize' => '5 MB',
      'max_resolution' => '3000x3000',
      'min_resolution' => '1400x1400',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 0,
          'image' => 'image',
          'pdf' => 0,
          'video' => 0,
        ),
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_item-field_simple_podcast_description'
  $field_instances['node-simple_podcast_item-field_simple_podcast_description'] = array(
    'bundle' => 'simple_podcast_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Full description.  This field allows some html tags, such as &lt;a&gt;, &lt;em&gt;, &lt;strong&gt;, but it is best to limit the types of html tags added to ensure maximum compatibility with various podcast clients.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_description',
    'label' => 'Description',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => '',
        'maxlength_js_enforce' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'maxlength_js_truncate_html' => 0,
        'rows' => 20,
      ),
      'type' => 'text_textarea',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'node-simple_podcast_item-field_simple_podcast_explicit'.
  $field_instances['node-simple_podcast_item-field_simple_podcast_explicit'] = array(
    'bundle' => 'simple_podcast_item',
    'default_value' => array(
      0 => array(
        'value' => 'no',
      ),
    ),
    'deleted' => 0,
    'description' => 'The most common value for this field is "No."  If you are unsure, leave the selected value set to "No."

Select "No" to in indicate that the podcast does not require an explicit language or adult content parental advisory notice, but also has not been certified as having absolutely no material that may be considered explicit or adult in nature to some recipients.
Select "Yes" if you wish to warn recipients of your podcast of the presence of explicit language or adult content.
Select "Clean" if you wish to certify that your podcast has absolutely no explicit language or adult content.
',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_explicit',
    'label' => 'Explicit',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_buttons',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'node-simple_podcast_item-field_simple_podcast_feed'.
  $field_instances['node-simple_podcast_item-field_simple_podcast_feed'] = array(
    'bundle' => 'simple_podcast_item',
    'default_value' => NULL,
    'default_value_function' => '',
    'deleted' => 0,
    'description' => 'Select one or more feeds for this podcast item to be included in.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_feed',
    'label' => 'Podcast Feed',
    'required' => 1,
    'settings' => array(
      'behaviors' => array(
        'prepopulate' => array(
          'status' => 0,
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_buttons',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_item-field_simple_podcast_file'
  $field_instances['node-simple_podcast_item-field_simple_podcast_file'] = array(
    'bundle' => 'simple_podcast_item',
    'deleted' => 0,
    'description' => 'Mp3 audio file.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_file',
    'label' => 'Podcast File',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'podcasts/files/[current-date:custom:Y/m/d]',
      'file_extensions' => 'mp3 mp4 pdf epub',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 'audio',
          'default' => 0,
          'image' => 0,
          'pdf' => 'pdf',
          'video' => 'video',
        ),
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_item-field_simple_podcast_summary'
  $field_instances['node-simple_podcast_item-field_simple_podcast_summary'] = array(
    'bundle' => 'simple_podcast_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A short description or subtitle of the podcast.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_summary',
    'label' => 'Summary',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 255,
        'maxlength_js_enforce' => 1,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'maxlength_js_truncate_html' => 0,
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-simple_podcast_item-field_simple_podcast_transcript'
  $field_instances['node-simple_podcast_item-field_simple_podcast_transcript'] = array(
    'bundle' => 'simple_podcast_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The full text transcript of the audio for the podcast.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_simple_podcast_transcript',
    'label' => 'Transcript',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => '',
        'maxlength_js_enforce' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'maxlength_js_truncate_html' => 0,
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 8,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A 255 character or less description of the podcast item.  Some HTML is allowed, but it is best to limit it to &lt;a&gt;, &lt;em&gt;, or &lt;strong&gt; tags, for best compatibility with podcast clients.');
  t('A short description or subtitle of the podcast.');
  t('Administrative contact for the podcast feed. ');
  t('Author');
  t('Copyright');
  t('Cover Art');
  t('Cover art image, with a minimum size of 1400 x 1400 pixels and a maximum size of 3000 x 3000 pixels, 72 dpi, in JPEG or PNG format with appropriate file extensions (.jpg, .png), and in the RGB colorspace.');
  t('Cover art must be in the JPEG or PNG file formats and in the RGB color space with a minimum size of 1400 x 1400 pixels and a maximum size of 3000 x 3000 pixels.');
  t('Custom Link');
  t('Description');
  t('Enter a custom URL and label for custom field elements.');
  t('Explicit');
  t('Full description.  This field allows some html tags, such as &lt;a&gt;, &lt;em&gt;, &lt;strong&gt;, but it is best to limit the types of html tags added to ensure maximum compatibility with various podcast clients.');
  t('Full text description of the Podcast feed.  Allows html tags, but for highest compatibility with podcast clients, it is best to stick to &lt;a&gt;, &lt;em&gt;, or &lt;strong&gt; tags.');
  t('Mp3 audio file.');
  t('Optional copyright information. Text from this field will be preceded with a &#169; symbol. If this field is empty, the copyright will be set to the current site\'s name.');
  t('Owner');
  t('Podcast Feed');
  t('Podcast File');
  t('Select from various automatically-generated subscribe links or add a custom link URL and label.

"iTunes" will result in an ITPC feed, for podcasts without an iTunes feed ID. Use a custom link if you have an iTunes feed ID.');
  t('Select one or more feeds for this podcast item to be included in.');
  t('Select the topic that best identifies the content associated with this feed.');
  t('Subscribe Links');
  t('Subscription Link Type');
  t('Summary');
  t('The full text transcript of the audio for the podcast.');
  t('The most common value for this field is "No."  If you are unsure, leave the selected value set to "No."

Select "No" to in indicate that the podcast does not require an explicit language or adult content parental advisory notice, but also has not been certified as having absolutely no material that may be considered explicit or adult in nature to some recipients.
Select "Yes" if you wish to warn recipients of your podcast of the presence of explicit language or adult content.
Select "Clean" if you wish to certify that your podcast has absolutely no explicit language or adult content.
');
  t('The name to show as the author of the overall feed.  Individual podcast items can have separate authors.');
  t('Topic');
  t('Transcript');

  return $field_instances;
}
